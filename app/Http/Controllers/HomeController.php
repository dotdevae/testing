<?php

namespace App\Http\Controllers;


use App\Post;
use Illuminate\Support\Facades\Request;

class HomeController extends Controller
{

    function index(){

        $posts = Post::paginate(1);

        return view("posts", ["posts" => $posts]);

    }

    function create(){

        $post = new Post();
        $post->title = "new post 3";
        $post->content = "new content 3 .......";
        $post->save();

    }


    function update(){

        $post = Post::find(1);

        $post->title = "a new title";

        $post->save();

    }


    function delete(){

        $post = Post::find(2);

        if($post->delete()){
            dd("post was deleted");
        }


        dd($post);

    }

    function redirect_me(){

        return redirect()->route("homepage");

    }


}
