<html>
    <head>
        <meta charset="utf-8" />
    </head>

    <body>

        <table>

            <tr>
                <td>
                    title
                </td>

                <td>
                    content
                </td>
            </tr>

            <?php foreach ($posts as $post){ ?>
            <tr>
                <td>
                    <?php echo $post->title; ?>
                </td>
                <td>
                    <?php echo $post->content; ?>
                </td>
            </tr>
            <?php } ?>

        </table>

        <?php echo $posts->links(); ?>

    </body>
</html>
